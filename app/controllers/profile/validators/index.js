const { validateChangePassword } = require('./validateChangePassword')
const { validateUpdateProfile } = require('./validateUpdateProfile')
const { validateCreateProfile } = require('./validateCreateProfile')
const { validateDeleteProfile } = require('./validateDeleteProfile')
const { validateUpdateEducationProfile } = require('./validateUpdateEducationProfile')

module.exports = {
  validateChangePassword,
  validateUpdateProfile,
  validateCreateProfile,
  validateDeleteProfile,
  validateUpdateEducationProfile
}

const { buildErrObject } = require('../../middleware/utils')
const User = require('../../models/libUser')
/**
 * Checks is password matches
 * @param {string} password - password
 * @param {Object} user - user object
 * @returns {boolean}
 */
const checkPassword = (password = '', user = {}) => {
  return new Promise((resolve, reject) => {
    User.findOne({ email: user.email, password }, async (err, isMatch) => {
      if (err) {
        return reject(buildErrObject(422, err.message))
      }
      if (!isMatch) {
        resolve(false)
      }
      resolve(true)
    })
  })
}

module.exports = { checkPassword }

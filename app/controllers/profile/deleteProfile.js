const { matchedData } = require('express-validator')
const { isIDGood, handleError } = require('../../middleware/utils')
const { deleteProfileInDB } = require('./helpers')


/**
 * Delete item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const deleteProfile = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await isIDGood(req.id)
    res.status(200).json(await deleteProfileInDB(id, req))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { deleteProfile }

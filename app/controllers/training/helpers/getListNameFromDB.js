const Training = require('../../../models/training')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Gets all items from database
 */
const getListNameFromDB = (id = '') => {
  return new Promise((resolve, reject) => {
    Training.find(
      {
        listnames: {
          $elemMatch: {
            _id: id
          }
        }
      },
      '-updatedAt -createdAt',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}

module.exports = { getListNameFromDB }

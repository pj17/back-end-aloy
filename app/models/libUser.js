const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const validator = require('validator')
const mongoosePaginate = require('mongoose-paginate-v2')

const EducationSchema = new mongoose.Schema({
  name: {
    type: String
  },
  abbreviation: {
    type: String
  },
  major: {
    type: String
  },
  institution: {
    type: String
  },
  year: {
    type: String
  }
})

const HistorySchema = new mongoose.Schema({
  directive: {
    type: String
  },
  date: {
    type: Date
  },
  position: {
    type: String
  },
  effectual: {
    type: Date
  },
  detail: {
    type: String
  },
  fiscalYear: {
    type: Number
  },
  condensed1: {
    type: String
  },
  condensed2: {
    type: String
  }

})

const SalarySchema = new mongoose.Schema({
  directive: {
    type: String
  },
  date: {
    type: Date
  },
  salary: {
    type: Number
  },
  effectual: {
    type: Date
  },
  detail: {
    type: String
  }
})

const InsigniasSchema = new mongoose.Schema({
  year: {
    type: Number
  },
  name: {
    type: String
  },
  condensed: {
    type: String
  }
})

const LibUserSchema = new mongoose.Schema(
  {
    prefix: {
      type: String,
      enum: ['นาย', 'นาง', 'นางสาว', 'ดร']
    },
    prefixEN: {
      type: String,
      enum: ['Mr', 'Miss', 'Mrs', 'Dr']
    },
    nameTH: {
      type: String,
      required: true
    },
    surnameTH: {
      type: String,
      required: true
    },
    nameEN: {
      type: String,
      required: true
    },
    surnameEN: {
      type: String,
      required: true
    },
    fullname: {
      type: String,
      required: true
    },
    birthdate: {
      type: Date
    },
    blood: {
      type: String,
      enum: ['ไม่ระบุ', 'เอ', 'บี', 'โอ', 'เอบี']
    },
    height: {
      type: String
    },
    weight: {
      type: String
    },
    religion: {
      type: String,
      enum: [
        'ไม่ระบุ',
        'ศาสนาพุทธ',
        'ศาสนาคริสต์',
        'ศาสนาอิสลาม',
        'ศาสนาพราหมณ์-ฮินดู'
      ]
    },
    status: {
      type: String,
      enum: ['ไม่ระบุ', 'โสด', 'คู่สมรส', 'หม้าย', 'หย่า', 'แยกกันอยู่']
    },
    nationality: {
      type: String
    },
    ethnicity: {
      type: String
    },
    idCard: {
      type: String,
      unique: true,
      required: true
    },
    type: {
      type: String
    },
    secondType: {
      type: String
    },
    address: {
      type: String
    },
    educations: [EducationSchema],
    historys: [HistorySchema],
    salarys: [SalarySchema],
    insignias: [InsigniasSchema],
    appointDay: {
      type: Date
    },
    serviceDay: {
      type: Date
    },
    sixtyDay: {
      type: Date
    },
    retirementDay: {
      type: Date
    },
    fiscalYear: {
      type: Number
    },
    ratenumber: {
      type: String
    },
    position: {
      type: String
    },
    division: {
      type: String
    },
    work: {
      type: String
    },
    campus: {
      type: String
    },
    affiliate: {
      type: String
    },
    phone: {
      type: String
    },
    email: {
      type: String,
      validate: {
        validator: validator.isEmail,
        message: 'EMAIL_IS_NOT_VALID'
      },
      lowercase: true,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true,
      select: false
    },
    role: {
      type: String,
      enum: ['user', 'admin'],
      default: 'user'
    },
    verification: {
      type: String
    },
    verified: {
      type: Boolean,
      default: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

const hash = (user, salt, next) => {
  bcrypt.hash(user.password, salt, (error, newHash) => {
    if (error) {
      return next(error)
    }
    user.password = newHash
    return next()
  })
}

const genSalt = (user, SALT_FACTOR, next) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

LibUserSchema.pre('save', function (next) {
  const that = this
  const SALT_FACTOR = 5
  if (!that.isModified('password')) {
    return next()
  }
  return genSalt(that, SALT_FACTOR, next)
})

LibUserSchema.methods.comparePassword = function (passwordAttempt, cb) {
  bcrypt.compare(passwordAttempt, this.password, (err, isMatch) =>
    err ? cb(err) : cb(null, isMatch)
  )
}
LibUserSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('LibUser', LibUserSchema)

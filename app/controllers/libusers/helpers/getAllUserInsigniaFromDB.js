const LibUser = require('../../../models/libUser')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Gets all items from database
 */
const getAllUserInsigniaFromDB = () => {
  return new Promise((resolve, reject) => {
    LibUser.find(
      { type: 'พนักงานมหาวิทยาลัย', secondType: 'งบแผ่นดิน'},
      '-updatedAt -createdAt',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}

module.exports = { getAllUserInsigniaFromDB }

const faker = require('faker')
const ObjectID = require('mongodb').ObjectID

module.exports = [
  {
    _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
    prefix: 'นาย',
    prefixEN: 'Mr',
    nameTH: 'ภูเบศ',
    surnameTH: 'สุขผึ้ง',
    nameEN: 'Phubet',
    surnameEN: 'Sukpueng',
    fullname: 'ภูเบศ สุขผึ้ง',
    birthdate: faker.date.past(),
    blood: 'เอ',
    height: '170',
    weight: '60',
    religion: 'ศาสนาพุทธ',
    status: 'คู่สมรส',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    idCard: '3200100609018',
    type: 'พนักงานมหาวิทยาลัย',
    secondType: 'งบมหาวิทยาลัย',

    address:
      '99/24 ม.6 ถ.มิตรสัมพันธ์ ต.บ้านปึก อ.เมืองชลบุรี จ.ชลบุรี 20130 โทรศัพท์: 0-3810-3060',

    educations: [
      {
        _id: new ObjectID('601bb3e04e684b298c24e744'),
        name: 'วิทยาศาสตรบัณฑิต',
        abbreviation: 'วท.บ.',
        major: 'วิทยาการคอมพิวเตอร์',
        institution: 'มหาวิทยาลัยบูรพา',
        year: '2540'
      }
    ],
    historys: [
      {
        _id: new ObjectID('602637d523140f1be859bad9'),
        directive: '047/2550',
        date: faker.date.past(),
        position: 'ผู้ปฎิบัติงานห้องสมุด ระดับ ปวส.ปวท.ป.กศ.สูง',
        effectual: faker.date.recent(),
        detail: 'บรรจุสอบคัดเลือก เลขที่อัตรา 9416',
        fiscalYear: '2540',
        condensed1: '-',
        condensed2: '-'
      }
    ],
    salarys: [
      {
        _id: new ObjectID('602637d523140f1be859bad9'),
        directive: '047/2550',
        date: faker.date.past(),
        salary: 6820.0,
        effectual: faker.date.recent(),
        detail: 'บรรจุสอบคัดเลือก'
      }
    ],
    insignias: [
      {
        year: 2565,
        name: 'เบญจมาภรณ์มงกุฎไทย',
        condensed: 'บ.ม.'
      },
      {
        year: 2570,
        name: 'เบญจมาภรณ์ช้างเผือก',
        condensed: 'บ.ช.'
      },
      {
        year: 2575,
        name: 'จัตุรถาภรณ์มงกุฎไทย',
        condensed: 'จ.ม.'
      },
      {
        year: 2580,
        name: 'จัตุรถาภรณ์ช้างเผือก',
        condensed: 'จ.ช.'
      }
    ],

    appointDay: new Date('1999-03-05'),
    serviceDay: new Date('1999-03-05'),
    sixtyDay: new Date('1999-03-05'),
    retirementDay: faker.date.recent(),
    fiscalYear: 2579,
    ratenumber: '745-2553',
    position: 'อาจารย์',
    division: '-',
    work: 'ตำแหน่งคณาจารย์',
    campus: 'บางแสน',
    affiliate: 'คณะวิทยาการสารสนเทศ',
    phone: '0831234567',

    email: '60160060@buu.ac.th',
    password: '@Apple4128t',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f'
  },
  {
    _id: new ObjectID('601bb07bf4910f0f94c605b2'),
    prefix: 'นาย',
    prefixEN: 'Mr',
    nameTH: 'เหมรัศมิ์',
    surnameTH: 'วชิรหัตถพงศ์',
    nameEN: 'Hemmarat',
    surnameEN: 'Wachirahattapong',
    fullname: 'เหมรัศมิ์ วชิรหัตถพงศ์',
    birthdate: faker.date.past(),
    blood: 'เอ',
    height: '170',
    weight: '60',
    religion: 'ศาสนาพุทธ',
    status: 'คู่สมรส',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    idCard: '1234567890123',
    type: 'พนักงานมหาวิทยาลัย',
    secondType: 'งบแผ่นดิน',

    address:
      '99/24 ม.6 ถ.มิตรสัมพันธ์ ต.บ้านปึก อ.เมืองชลบุรี จ.ชลบุรี 20130 โทรศัพท์: 0-3810-3060',

    educations: [
      {
        _id: new ObjectID('60015a2447fecb1090335c45'),
        name: 'วิทยาศาสตรบัณฑิต',
        abbreviation: 'วท.บ.',
        major: 'วิทยาการคอมพิวเตอร์',
        institution: 'มหาวิทยาลัยบูรพา',
        year: '2540'
      },
      {
        _id: new ObjectID('60015a378001f720b8d35345'),
        name: 'วิทยาศาสตรมหาบัณฑิต',
        abbreviation: 'วท.ม.',
        major: 'เทคโนโลยีสารสนเทศ',
        institution: 'สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง',
        year: '2542'
      }
    ],
    historys: [
      {
        _id: new ObjectID('602637d523140f1be859bad9'),
        directive: '047/2550',
        date: faker.date.past(),
        position: 'ผู้ปฎิบัติงานห้องสมุด ระดับ ปวส.ปวท.ป.กศ.สูง',
        effectual: faker.date.recent(),
        detail: 'บรรจุสอบคัดเลือก เลขที่อัตรา 9416',
        fiscalYear: '2540',
        condensed1: '-',
        condensed2: '-'
      }
    ],
    salarys: [
      {
        _id: new ObjectID('602637d523140f1be859bad9'),
        directive: '028/2551',
        date: faker.date.past(),
        salary: 7100.0,
        effectual: faker.date.recent(),
        detail: 'ปรับเงินเดือนพนักงานมหาวิทยาลัย(เงินรายได้)'
      }
    ],
    insignias: [
      {
        year: 2568,
        name: 'เบญจมาภรณ์มงกุฎไทย',
        condensed: 'บ.ม.'
      },
      {
        year: 2573,
        name: 'เบญจมาภรณ์ช้างเผือก',
        condensed: 'บ.ช.'
      },
      {
        year: 2578,
        name: 'จัตุรถาภรณ์มงกุฎไทย',
        condensed: 'จ.ม.'
      },
      {
        year: 2583,
        name: 'จัตุรถาภรณ์ช้างเผือก',
        condensed: 'จ.ช.'
      }
    ],
    appointDay: new Date('1999-03-05'),
    serviceDay: faker.date.recent(),
    sixtyDay: faker.date.recent(),
    retirementDay: faker.date.recent(),
    fiscalYear: 2579,
    ratenumber: '745-2553',
    position: 'อาจารย์',
    division: '-',
    work: 'ตำแหน่งคณาจารย์',
    campus: 'บางแสน',
    affiliate: 'คณะวิทยาการสารสนเทศ',
    phone: '0937654321',

    email: 'admin@admin.com',
    password: '12345',
    role: 'admin',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f'
  }
  , {
    _id: new ObjectID('605f1386dfac562efc4cef4d'),
    prefix: 'นาย',
    prefixEN: 'Mr',
    nameTH: 'กฤตวัฒน์',
    surnameTH: 'เกิดศิริ',
    nameEN: 'KITTAWAT',
    surnameEN: 'KERDSIRI',
    fullname: 'กฤตวัฒน์ เกิดศิริ',
    birthdate: faker.date.past(),
    blood: 'บี',
    height: '175',
    weight: '61',
    religion: 'ศาสนาพุทธ',
    status: 'โสด',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    idCard: '3245500122897',
    type: 'พนักงานมหาวิทยาลัย',
    secondType: 'งบแผ่นดิน',
    address:
      '42/1 หมู่ 1 ตำบล บางพระ อำเภอศรีราชา ชลบุรี 20110',

    educations: [
      {
        _id: new ObjectID('605f138bfc2a3d4ef8a3cdda'),
        name: 'วิทยาศาสตรบัณฑิต',
        abbreviation: 'วท.บ.',
        major: 'วิทยาการคอมพิวเตอร์',
        institution: 'มหาวิทยาลัยบูรพา',
        year: '2540'
      },
      {
        _id: new ObjectID('605f15507e281f5b94bdd080'),
        name: 'วิทยาศาสตรมหาบัณฑิต',
        abbreviation: 'วท.ม.',
        major: 'เทคโนโลยีสารสนเทศ',
        institution: 'สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง',
        year: '2544'
      }
    ],
    historys: [
      {
        _id: new ObjectID('605f156306737d1e105971a6'),
        directive: '040/2552',
        date: faker.date.past(),
        position: 'ผู้ปฎิบัติงานห้องสมุด ระดับ ปวส.ปวท.ป.กศ.สูง',
        effectual: faker.date.recent(),
        detail: 'บรรจุสอบคัดเลือก เลขที่อัตรา 9242',
        fiscalYear: '2544',
        condensed1: '-',
        condensed2: '-'
      }
    ],
    salarys: [
      {
        _id: new ObjectID('605f1d2e76b3a54c6cce28e9'),
        directive: '040/2552',
        date: faker.date.past(),
        salary: 8000,
        effectual: faker.date.recent(),
        detail: 'ปรับเงินเดือนพนักงานมหาวิทยาลัย(เงินรายได้)'
      }
    ],
    insignias: [
      {
        year: 2558,
        name: 'เบญจมาภรณ์มงกุฎไทย',
        condensed: 'บ.ม.'
      },
      {
        year: 2563,
        name: 'เบญจมาภรณ์ช้างเผือก',
        condensed: 'บ.ช.'
      },
      {
        year: 2568,
        name: 'จัตุรถาภรณ์มงกุฎไทย',
        condensed: 'จ.ม.'
      },
      {
        year: 2573,
        name: 'จัตุรถาภรณ์ช้างเผือก',
        condensed: 'จ.ช.'
      }
    ],
    appointDay: new Date('1999-03-05'),
    serviceDay: faker.date.recent(),
    sixtyDay: faker.date.recent(),
    retirementDay: faker.date.recent(),
    fiscalYear: 2579,
    ratenumber: '040/2552',
    position: 'อาจารย์',
    division: '-',
    work: 'ตำแหน่งคณาจารย์',
    campus: 'บางแสน',
    affiliate: 'คณะวิทยาการสารสนเทศ',
    phone: '0804422109',

    email: 'Kittawat@hotmail.com',
    password: '12345',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f'
  }
  , {
    _id: new ObjectID('605f17b9b27085472cd204d8'),
    prefix: 'นาย',
    prefixEN: 'Mr',
    nameTH: 'ศิรวิทย์',
    surnameTH: 'บุ้งทอง',
    nameEN: 'SIRAWIT',
    surnameEN: 'BOONGTHONG',
    fullname: 'ศิรวิทย์ บุ้งทอง',
    birthdate: faker.date.past(),
    blood: 'โอ',
    height: '173',
    weight: '69',
    religion: 'ศาสนาคริสต์',
    status: 'คู่สมรส',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    idCard: '1248811562301',
    type: 'พนักงานมหาวิทยาลัย',
    secondType: 'งบแผ่นดิน',
    address:
      '22/2 หมู่ 3 ถนน เลียบทางรถไฟ เมืองพัทยา อำเภอบางละมุง ชลบุรี 20150',

    educations: [
      {
        _id: new ObjectID('605f187bb5ac481bf0b92d14'),
        name: 'วิทยาศาสตรบัณฑิต',
        abbreviation: 'วท.บ.',
        major: 'วิทยาการคอมพิวเตอร์',
        institution: 'มหาลัยธนบุรี',
        year: '2540'
      }
    ],
    historys: [
      {
        _id: new ObjectID('605f18b65b3db95acc2ed00d'),
        directive: '780/2555',
        date: faker.date.past(),
        position: 'ผู้ช่วยปฎิบัติงานห้องสมุด',
        effectual: faker.date.recent(),
        detail: 'บรรจุสอบคัดเลือก เลขที่อัตรา 8855',
        fiscalYear: '2544',
        condensed1: '-',
        condensed2: '-'
      }
    ],
    salarys: [
      {
        _id: new ObjectID('605f159da664de5c0cf7f88f'),
        directive: '780/2555',
        date: faker.date.past(),
        salary: 10000,
        effectual: faker.date.recent(),
        detail: 'ปรับเงินเดือนพนักงานมหาวิทยาลัย(เงินรายได้)'
      }
    ],
    insignias: [
      {
        year: 2557,
        name: 'เบญจมาภรณ์มงกุฎไทย',
        condensed: 'บ.ม.'
      },
      {
        year: 2562,
        name: 'เบญจมาภรณ์ช้างเผือก',
        condensed: 'บ.ช.'
      },
      {
        year: 2567,
        name: 'จัตุรถาภรณ์มงกุฎไทย',
        condensed: 'จ.ม.'
      },
      {
        year: 2572,
        name: 'จัตุรถาภรณ์ช้างเผือก',
        condensed: 'จ.ช.'
      }
    ],
    appointDay: new Date('2013-03-05'),
    serviceDay: faker.date.recent(),
    sixtyDay: faker.date.recent(),
    retirementDay: faker.date.recent(),
    fiscalYear: 2579,
    ratenumber: '780/2555',
    position: 'ผู้ช่วยอาจารย์',
    division: 'ผู้จัดการทั่วไป',
    work: 'ตำแหน่งคณาจารย์',
    campus: 'บางแสน',
    affiliate: 'คณะวิทยาการสารสนเทศ',
    phone: '0896661221',

    email: 'SIRAWIT@hotmail.com',
    password: '12345',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f'
  }
  , {
    _id: new ObjectID('605f1d5e1a05e91cbc77f57c'),
    prefix: 'นาง',
    prefixEN: 'Mrs',
    nameTH: 'จุลรักษ์',
    surnameTH: 'จันทร์ดี',
    nameEN: 'JULARUK',
    surnameEN: 'JANDEE',
    fullname: 'จุลรักษ์ จันทร์ดี',
    birthdate: faker.date.past(),
    blood: 'เอบี',
    height: '159',
    weight: '43',
    religion: 'ศาสนาคริสต์',
    status: 'คู่สมรส',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    idCard: '3235512785051',
    type: 'พนักงานมหาวิทยาลัย',
    secondType: 'งบแผ่นดิน',
    address:
      '3406 ตำบล นายายอาม อำเภอนายายอาม จันทบุรี 22160',

    educations: [
      {
        _id: new ObjectID('605f1ec2b777a93a241280a6'),
        name: 'วิทยาศาสตรบัณฑิต',
        abbreviation: 'วท.บ.',
        major: 'วิทยาการคอมพิวเตอร์',
        institution: 'มหาลัยศรีปทุม',
        year: '2545'
      }
    ],
    historys: [

    ],
    salarys: [
      {
        _id: new ObjectID('605f159da664de5c0cf7f88f'),
        directive: '180/2546',
        date: faker.date.past(),
        salary: 10000,
        effectual: faker.date.recent(),
        detail: 'ปรับเงินเดือนพนักงานมหาวิทยาลัย(เงินรายได้)'
      }
    ],
    insignias: [
      {
        year: 2550,
        name: 'เบญจมาภรณ์มงกุฎไทย',
        condensed: 'บ.ม.'
      },
      {
        year: 2555,
        name: 'เบญจมาภรณ์ช้างเผือก',
        condensed: 'บ.ช.'
      },
      {
        year: 2560,
        name: 'จัตุรถาภรณ์มงกุฎไทย',
        condensed: 'จ.ม.'
      },
      {
        year: 2565,
        name: 'จัตุรถาภรณ์ช้างเผือก',
        condensed: 'จ.ช.'
      }
    ],
    appointDay: new Date('2013-03-05'),
    serviceDay: faker.date.recent(),
    sixtyDay: faker.date.recent(),
    retirementDay: faker.date.recent(),
    fiscalYear: 2580,
    ratenumber: '180/2546',
    position: 'อาจารย์',
    division: '-',
    work: 'ตำแหน่งคณาจารย์',
    campus: 'บางแสน',
    affiliate: 'คณะวิทยาการสารสนเทศ',
    phone: '0891115674',
    email: 'JULARUK@hotmail.com',
    password: '12345',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f'
  }
]

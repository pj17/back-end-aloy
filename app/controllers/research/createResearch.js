const LibUser = require('../../models/libUser')
const { createItemInDb } = require('./helpers')
const { handleError } = require('../../middleware/utils')
const { matchedData } = require('express-validator')
const { updateResearchInDb } = require('./helpers')

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createResearch = async (req, res) => {
  try {
    req = matchedData(req)
    res.status(201).json(await createItemInDb(req))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createResearch }

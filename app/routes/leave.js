const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const { roleAuthorization } = require('../controllers/auth')

const {
  getAllLeave,
  getLeaves,
  createLeave,
  getLeave,
  updateLeave,
  deleteLeave,
  getLeaveUser
} = require('../controllers/leave')

const {
  validateCreateLeave,
  validateGetLeave,
  validateUpdateLeave,
  validateDeleteLeave
} = require('../controllers/leave/validators')

/*
 * Cities routes
 */

/*
 * Get all items route
 */
router.get('/all', getAllLeave)

/*
 * Get items route
 */
router.get(
  '/',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  getLeaves
)

/*
 * Create new item route
 */
router.post(
  '/',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateCreateLeave,
  createLeave
)

/*
 * Get item route
 */
router.get(
  '/:id',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateGetLeave,
  getLeave
)

/*
 * Get item User route
 */
router.get(
  '/user/:id',
  requireAuth,
  roleAuthorization(['user','admin']),
  trimRequest.all,
  validateGetLeave,
  getLeaveUser
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateUpdateLeave,
  updateLeave
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateDeleteLeave,
  deleteLeave
)

module.exports = router

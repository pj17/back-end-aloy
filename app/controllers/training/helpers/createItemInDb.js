// const { id } = require('date-fns/locale')
// const { name } = require('faker')
const { buildErrObject } = require('../../../middleware/utils')
const Training = require('../../../models/training')

/**
 * Creates a new item in database
 * @param {string} id - item id
 * @param {Object} req - request object
 */

const createItemInDb = (
  {
    topic = '',
    startDate = '',
    endDate = '',
    location = '',
    description = '',
    file = '',
    listnames = []

  }
) => {
  return new Promise((resolve, reject) => {
    const training = new Training({
      topic,
      startDate,
      endDate,
      location,
      description,
      file,
      listnames

    })
    training.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

       
      
      resolve(item)
    })
  })
}

module.exports = { createItemInDb }

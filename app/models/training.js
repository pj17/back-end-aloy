const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ListnameSchema = new mongoose.Schema({
  name: {
    type: String
  }
})

const TrainingSchema = new mongoose.Schema(
  {
    topic: {
      type: String,
      required: true
    },
    startDate: {
      type: Date,
      required: true
    },
    endDate: {
      type: Date,
      required: true
    },
    location: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    file: {
      type: String,
      required: true
    },
    listnames: [ListnameSchema]
  },
  {
    versionKey: false,
    timestamps: true
  }
)
TrainingSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Training', TrainingSchema)

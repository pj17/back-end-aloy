const { matchedData } = require('express-validator')
const Research = require('../../models/research')
const { getItemFromDB } = require('./helpers')
const { isIDGood, handleError } = require('../../middleware/utils')

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getResearchUser = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await isIDGood(req.id)
    res.status(200).json(await getItemFromDB(id, Research))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getResearchUser }

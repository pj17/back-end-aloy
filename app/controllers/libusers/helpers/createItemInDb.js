/* eslint-disable camelcase */
const uuid = require('uuid')
const LibUser = require('../../../models/libUser')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Creates a new item in database
 * @param {Object} req - request object
 */
const createItemInDb = ({
  prefix = '',
  prefixEN = '',
  nameTH = '',
  surnameTH = '',
  nameEN = '',
  surnameEN = '',
  fullname = '',
  birthdate = '',
  blood = '',
  height = '',
  weight = '',
  religion = '',
  status = '',
  nationality = '',
  ethnicity = '',
  idCard = '',
  type = '',
  secondType = '',
  address = '',
  appointDay = '',
  serviceDay = '',
  sixtyDay = '',
  retirementDay = '',
  fiscalYear = 0,
  ratenumber = '',
  position = '',
  division = '',
  work = '',
  campus = '',
  affiliate = '',
  phone = '',
  email = '',
  password = '',
  role = '',
  educations = [],
  historys = [],
  salarys = [],
  insignias = []


}) => {
  return new Promise((resolve, reject) => {
    const libUser = new LibUser({
      prefix,
      prefixEN,
      nameTH,
      surnameTH,
      nameEN,
      surnameEN,
      fullname,
      birthdate,
      blood,
      height,
      weight,
      religion,
      status,
      nationality,
      ethnicity,
      idCard,
      type,
      secondType,
      address,
      appointDay,
      serviceDay,
      sixtyDay,
      retirementDay,
      fiscalYear,
      ratenumber,
      position,
      division,
      work,
      campus,
      affiliate,
      phone,
      email,
      password,
      role,
      educations,
      historys,
      salarys,
      insignias,
      verification: uuid.v4()
    })
    libUser.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

      resolve(item)
    })
  })
}

module.exports = { createItemInDb }

const { handleError } = require('../../middleware/utils')
const { getAllUserInsigniaFromDB } = require('./helpers')

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getInsigniaUsers = async (req, res) => {
  try {
    res.status(200).json(await getAllUserInsigniaFromDB())
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getInsigniaUsers }

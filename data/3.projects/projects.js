const faker = require('faker')

const json = [
    {
    name: 'Mock Project Test 1',
    description : 'A Test Mock Up Descriptions 1',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mock Project Test 2',
    description : 'A Test Mock Up Descriptions 2',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  }
]

module.exports = json

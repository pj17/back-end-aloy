const ObjectID = require('mongodb').ObjectID

const json = [
  {
    titleThai: 'การใช้เทคโนโลยีสารสนเทศและ การสื่อสารในองค์กร',
    titleEnglish: 'How to Use the Information Communication Technology',
    institute: 'มหาวิทยาลัยบูรพา',
    work: 'คณะวิทยาการสารสนเทศ',
    type: 'เงินรายได้',
    researchFund: 'ทุนภายใน',
    yearScholarship: '2554',
    expenditure: 10000,
    objective: 'วัตถุประสงค์',
    strategy: 'ยุทธศาสตร์',

    participants: [
      {
        _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
        name: 'ภูเบศ สุขผึ้ง',
        position: 'หัวหน้าโครงการวิจัย',
        percentWork: '100'
      }
    ],
    abstract:
      'วิธีการของการตรวจจับการบุกรุกสามารถแบ่งออกได้เป็น 2 ชนิด คือ วิธีการตรวจจับการบุกรุกแบบอโนมาลี (anomaly intrusion detection method) และวิธีการตรวจจับการบุกรุกแบบมิสยูส (misuse intrusion detection method) ',
    relatedFile: 'Name.txt'
  },
  {
    titleThai: 'พฤติกรรมการใช้เทคโนโลยีสารสนเทศของนิสิตระดับปริญญาตรี',
    titleEnglish: 'Information Technology Use Behavior of Undergraduates',
    institute: 'มหาวิทยาลัยบูรพา',
    work: 'คณะวิทยาการสารสนเทศ',
    type: 'เงินรายได้',
    researchFund: 'ทุนภายใน',
    yearScholarship: '2559',
    expenditure: 20000,
    objective: 'วัตถุประสงค์',
    strategy: 'ยุทธศาสตร์',

    participants: [
      {
        _id: new ObjectID('601bb07bf4910f0f94c605b2'),
        name: 'เหมรัศมิ์ วชิรหัตถพงศ์',
        position: 'หัวหน้าโครงการวิจัย',
        percentWork: '50'
      },
      {
        _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
        name: 'ภูเบศ สุขผึ้ง',
        position: 'ผู้ส่วนร่วมโครงการวิจัย',
        percentWork: '50'
      }
    ],
    abstract:
      'การหาคุณลักษณะของชุดข้อมูลที่สามารถแทนข้อมูลได้และมีข้อจำนวนคุณลักษณะที่เหมาะสม และขั้นตอนที่ทำ',
    relatedFile: 'Word.txt',
  },
  {
    titleThai: 'การหาแนวทางเพื่อพัฒนาการใช้เทคโนโลยีสารสนเทศสำหรับการจัดการความรู้ในองค์กรธุรกิจขนาดกลาง',
    titleEnglish: 'The development of Information Technology Application for Knowledge Management in Medium-sized Business Organizations',
    institute: 'มหาวิทยาลัยบูรพา',
    work: 'คณะการจัดการและการท่องเที่ยว',
    type: 'เงินรายได้',
    researchFund: 'ทุนภายนอก',
    yearScholarship: '2560',
    expenditure: 30000,
    objective: 'วัตถุประสงค์',
    strategy: 'ยุทธศาสตร์',

    participants: [
      {
        _id: new ObjectID('601bb07bf4910f0f94c605b2'),
        name: 'เหมรัศมิ์ วชิรหัตถพงศ์',
        position: 'หัวหน้าโครงการวิจัย',
        percentWork: '100'
      }
    ],
    abstract:
      'การหาคุณลักษณะของชุดข้อมูลที่สามารถแทนข้อมูลได้และมีข้อจำนวนคุณลักษณะที่เหมาะสม และขั้นตอนที่ทำ',
    relatedFile: 'Phone.txt',
  }
]

module.exports = json

const libUser = require('../../../models/libUser')
const { itemNotFound } = require('../../../middleware/utils')

/**
 * Finds user by email
 * @param {string} email - user´s email
 * @param {string} password - user´s password

 */
const findUser = (email = '', password = '') => {
  return new Promise((resolve, reject) => {
    const fullMail = email + '@admin.com'
    libUser.findOne(
      {
        email : fullMail
      },
      'password loginAttempts blockExpires name email role verified verification',
      async (err, item) => {
        try {
          await itemNotFound(err, item, 'USER_DOES_NOT_EXIST')
          resolve(item)
        } catch (error) {
          reject(error)
        }
      }
    )
  })
}

module.exports = { findUser }

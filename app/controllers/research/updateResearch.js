// const { updateItem } = require('../../middleware/db')
const LibUser = require('../../models/libUser')
const Research = require('../../models/research')

const { isIDGood, handleError } = require('../../middleware/utils')
const { matchedData } = require('express-validator')
const { createItemInDb, updateResearchInDb } = require('./helpers')
const { deleteItem } = require('../../middleware/db')
/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const updateResearch = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await isIDGood(req.id)
    res.status(200).json(await updateResearchInDb(id, Research, req))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { updateResearch }

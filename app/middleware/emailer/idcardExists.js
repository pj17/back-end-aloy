const LibUser = require('../../models/libUser')
const { buildErrObject } = require('../utils')

/**
 * Checks User model if user with an specific email exists
 * @param {string} idCard - user email
 */
const idcardExists = (idCard = '') => {
  return new Promise((resolve, reject) => {
    LibUser.findOne(
      {
        idCard
      },
      (err, item) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }

        if (item) {
          return reject(buildErrObject(422, 'idCard_ALREADY_EXISTS'))
        }
        resolve(false)
      }
    )
  })
}

module.exports = { idcardExists }

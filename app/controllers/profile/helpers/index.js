const { changePasswordInDB } = require('./changePasswordInDB')
const { findUser } = require('./findUser')
const { getProfileFromDB } = require('./getProfileFromDB')
const { updateProfileInDB } = require('./updateProfileInDB')
const { createProfileInDB } = require('./createProfileInDB')
const { deleteProfileInDB } = require('./deleteProfileInDB')
const { editProfileInDB } = require('./editProfileInDB')

module.exports = {
  changePasswordInDB,
  findUser,
  getProfileFromDB,
  updateProfileInDB,
  createProfileInDB,
  deleteProfileInDB,
  editProfileInDB
}

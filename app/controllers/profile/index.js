const { changePassword } = require('./changePassword')
const { getProfile } = require('./getProfile')
const { updateProfile } = require('./updateProfile')
const { createProfile } = require('./createProfile')
const { deleteProfile} = require('./deleteProfile')
const { updateEducationProfile} = require('./updateEducationProfile')


module.exports = {
  changePassword,
  getProfile,
  updateProfile,
  createProfile,
  deleteProfile,
  updateEducationProfile
}

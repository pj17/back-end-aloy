const { createResearch } = require('./createResearch')
const { deleteResearch } = require('./deleteResearch')
const { getAllResearch } = require('./getAllResearch')
const { getResearch } = require('./getResearch')
const { getResearchs } = require('./getResearchs')
const { updateResearch } = require('./updateResearch')
const { getResearchUser } = require('./getResearchUser')


module.exports = {
  createResearch,
  deleteResearch,
  getAllResearch,
  getResearch,
  getResearchs,
  updateResearch,
  getResearchUser
}

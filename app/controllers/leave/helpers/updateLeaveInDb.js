const { itemNotFound } = require('../../../middleware/utils')

/**
 * Updates an item in database by id
 * @param {string} id - item id
 * @param {Object} req - request object
 */
const updateLeaveInDb = (req = {}, model = {}) => {
  return new Promise((resolve, reject) => {
    model.findOne(
      {fullname: req.fullname},
      {
        new: true,
        runValidators: true
      },
      async (err, item) => {
        try {
          await itemNotFound(err, item, 'NOT_FOUND')
          resolve(item.id)
        } catch (error) {
          reject(error)
        }
      }
    )
  })
}

module.exports = { updateLeaveInDb }

const { isIDGood, handleError } = require('../../middleware/utils')
const { matchedData } = require('express-validator')
const { createProfileInDB } = require('./helpers')

/**
 * Update profile function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createProfile = async (req, res) => {
  try {
    const id = await isIDGood(req.user._id)
    req = matchedData(req)
    res.status(200).json(await createProfileInDB(req, id))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createProfile }

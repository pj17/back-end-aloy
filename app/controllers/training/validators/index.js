const { validateCreateTraining } = require('./validateCreateTraining')
const { validateDeleteTraining } = require('./validateDeleteTraining')
const { validateGetTraining } = require('./validateGetTraining')
const { validateUpdateTraining } = require('./validateUpdateTraining')
const { validateGetUserTraining } = require('./validateGetUserTraining')


module.exports = {
  validateCreateTraining,
  validateDeleteTraining,
  validateGetTraining,
  validateUpdateTraining,
  validateGetUserTraining
}

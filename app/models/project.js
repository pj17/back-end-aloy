const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ProjectSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
ProjectSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Project', ProjectSchema)


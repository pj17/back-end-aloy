const { buildErrObject } = require('../../../middleware/utils')
const Research = require('../../../models/research')

/**
 * Creates a new item in database
 * @param {string} id - item id
 * @param {Object} req - request object
 */

const createItemInDb = (
  {
    titleThai = '',
    titleEnglish = '',
    institute = '',
    work = '',
    type = '',
    researchFund = '',
    yearScholarship = '',
    expenditure = '',
    objective = '',
    strategy = '',
    participants = [],
    abstract = '',
    relatedFile = ''
  }
) => {
  return new Promise((resolve, reject) => {
    const research = new Research({
      titleThai,
      titleEnglish,
      institute,
      work,
      type,
      researchFund,
      yearScholarship,
      expenditure,
      objective,
      strategy,
      participants,
      abstract,
      relatedFile
    })
    research.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

      resolve(item)
    })
  })
}

module.exports = { createItemInDb }

const { createTraining } = require('./createTraining')
const { deleteTraining } = require('./deleteTraining')
const { getAllTrainings } = require('./getAllTrainings')
const { getTraining } = require('./getTraining')
const { getTrainings } = require('./getTrainings')
const { updateTraining } = require('./updateTraining')
const { getUserTrainings } = require('./getUserTrainings')

module.exports = {
  createTraining,
  deleteTraining,
  getAllTrainings,
  getTraining,
  getTrainings,
  updateTraining,
  getUserTrainings
}

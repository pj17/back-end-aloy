const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const { getListNameFromDB } = require('./helpers')

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getUserTrainings = async (req, res) => {
  try {
    req = matchedData(req)
     res.status(200).json(await getListNameFromDB(req.id))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getUserTrainings }

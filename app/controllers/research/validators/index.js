const { validateCreateResearch } = require('./validateCreateResearch')
const { validateDeleteResearch } = require('./validateDeleteResearch')
const { validateGetResearch } = require('./validateGetResearch')
const { validateUpdateResearch } = require('./validateUpdateResearch')

module.exports = {
  validateCreateResearch,
  validateDeleteResearch,
  validateGetResearch,
  validateUpdateResearch
}

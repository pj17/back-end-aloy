const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const json = [
  {
    topic: 'ตัวอย่างชื่อหัวข้อการอบรมครั้งที่1',
    startDate: faker.date.recent(),
    endDate: faker.date.recent(),
    location: 'ชั้น3 ห้องเรียนIF-3C04 อาคารคณะวิทยาการสารสนเทศ',
    description: 'รายละเอียดการอบรมครั้งที่1',
    file: 'ChineseDay_2564.docx',
    listnames: [
      {
        _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
        name: 'ภูเบศ สุขผึ้ง'
      },
      {
        _id: new ObjectID('601bb07bf4910f0f94c605b2'),
        name: 'เหมรัศมิ์ วชิรหัตถพงศ์'
      }
    ]
  },
  {
    topic: 'ตัวอย่างชื่อหัวข้อการอบรมครั้งที่2',
    startDate: faker.date.recent(),
    endDate: faker.date.recent(),
    location: 'ชั้น2 อาคารสำนักหอสมุดมหาวิทยาลัยบูรพา',
    description: 'รายละเอียดการอบรมครั้งที่2',
    file: 'logo.png',
    listnames: [
      {
        _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
        name: 'ภูเบศ สุขผึ้ง'
      }
    ]
  },
  {
    topic: 'ตัวอย่างชื่อหัวข้อการอบรมครั้งที่3',
    startDate: faker.date.recent(),
    endDate: faker.date.recent(),
    location: 'ชั้น4 อาคารสำนักหอสมุดมหาวิทยาลัยบูรพา',
    description: 'รายละเอียดการอบรมครั้งที่3',
    file: 'address.txt',
    listnames: [
      {
        _id: new ObjectID('601bb07bf4910f0f94c605b2'),
        name: 'เหมรัศมิ์ วชิรหัตถพงศ์'
      }
    ]
  }
]

module.exports = json

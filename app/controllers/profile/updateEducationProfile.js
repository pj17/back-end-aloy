const { matchedData } = require('express-validator')
const { isIDGood, handleError } = require('../../middleware/utils')
const { deleteProfileInDB } = require('./helpers')
const { editProfileInDB } = require('./helpers')


/**
 * Delete item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const updateEducationProfile = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await isIDGood(req.id)
    await deleteProfileInDB(id, req)
    res.status(200).json(await editProfileInDB(req, id))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { updateEducationProfile }

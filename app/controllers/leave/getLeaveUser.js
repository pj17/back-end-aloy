const { matchedData } = require('express-validator')
const Leave = require('../../models/leave')
const { getItemFromDB } = require('./helpers')
const { isIDGood, handleError } = require('../../middleware/utils')

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getLeaveUser = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await isIDGood(req.id)
    res.status(200).json(await getItemFromDB(id, Leave))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getLeaveUser }

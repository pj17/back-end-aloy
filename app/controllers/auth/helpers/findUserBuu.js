const libUser = require('../../../models/libUser')
const { itemNotFound } = require('../../../middleware/utils')

/**
 * Finds user by email
 * @param {string} email - user´s email
 * @param {string} password - user´s password

 */
const findUserBuu = (email = '', password = '') => {
  return new Promise((resolve, reject) => {
    const fullMail = email + '@buu.ac.th'
    libUser.findOneAndUpdate(
      {
        email: fullMail
      },
      {
        $set: { password: password }
      },
      {
        new: true
      },
      async (err, item) => {
        try {
          await itemNotFound(err, item, 'USER_DOES_NOT_EXIST')
          resolve(item)
        } catch (error) {
          reject(error)
        }
      }
    )
  })
}

module.exports = { findUserBuu }

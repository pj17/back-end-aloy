const LibUser = require('../../models/libUser')
const { buildErrObject } = require('../utils')

/**
 * Checks if a city already exists excluding itself
 * @param {string} id - id of item
 * @param {string} idCard
 */
const idcardExistsExcludingItself = (id = '', idCard = '') => {
  return new Promise((resolve, reject) => {
    LibUser.findOne(
      {
        idCard,
        _id: {
          $ne: id
        }
      },
      (err, item) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }

        if (item) {
          return reject(buildErrObject(422, 'IDCARD_ALREADY_EXISTS'))
        }

        resolve(false)
      }
    )
  })
}

module.exports = { idcardExistsExcludingItself }

const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ParticipantSchema = new mongoose.Schema({
  name: {
    type: String
  },
  position: {
    type: String
  },
  percentWork: {
    type: String
  }
})

const ResearchSchema = new mongoose.Schema(
  {
    titleThai: {
      type: String,
      required: true
    },
    titleEnglish: {
      type: String,
      required: true
    },
    institute: {
      type: String,
      required: true
    },
    work: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    },
    researchFund: {
      type: String,
      required: true
    },
    yearScholarship: {
      type: String,
      required: true
    },
    objective: {
      type: String,
      required: true
    },
    strategy: {
      type: String,
      required: true
    },
    expenditure: {
      type: Number,
      required: true
    },
    participants: [ParticipantSchema],
    abstract: {
      type: String,
      required: true
    },
    relatedFile: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
ResearchSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Researchs', ResearchSchema)

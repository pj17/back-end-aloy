const { buildErrObject } = require('../../../middleware/utils')
const Leave = require('../../../models/leave')

/**
 * Creates a new item in database
 * @param {string} id - item id
 * @param {Object} req - request object
 */

const createItemInDb = ({
  fullname = '',
  type = '',
  start = '',
  end = '',
  quantity = 0,
  description = '',
  certificate = '',
  _foreignKey = ''
}) => {
  return new Promise((resolve, reject) => {
    const leave = new Leave({
      fullname,
      type,
      start,
      end,
      quantity,
      description,
      certificate,
      _foreignKey
    })
    leave.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

      // delete item.password
      // delete item.blockExpires
      // delete item.loginAttempts

      resolve(item)
    })
  })
}

module.exports = { createItemInDb }

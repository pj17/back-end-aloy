const { createLeave } = require('./createLeave')
const { deleteLeave } = require('./deleteLeave')
const { getAllLeave } = require('./getAllLeave')
const { getLeave } = require('./getLeave')
const { getLeaves } = require('./getLeaves')
const { updateLeave } = require('./updateLeave')
const { getLeaveUser } = require('./getLeaveUser')


module.exports = {
  createLeave,
  deleteLeave,
  getAllLeave,
  getLeave,
  getLeaves,
  updateLeave,
  getLeaveUser
}

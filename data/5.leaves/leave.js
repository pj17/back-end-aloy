const faker = require('faker')
const ObjectID = require('mongodb').ObjectID

const json = [
  {
    fullname: 'ภูเบศ สุขผึ้ง',
    type: 'Sick Leave',
    start: faker.date.past(),
    end: faker.date.recent(),
    quantity: 2,
    description:
      'ปวดหัว ตามัว ตัวร้อน ไม่ได้พักไม่ได้ผ่อน ผอมแห้งแรงน้อย กินไม่ได้ นอนไม่หลับ',
    certificate: 'มีใบรับรองแพทย์',
    _foreignKey: new ObjectID('5fdc5ddc8127c53ddce25701')
  },
  {
    fullname: 'เหมรัศมิ์ วชิรหัตถพงศ์',
    type: 'Sick Leave',
    start: faker.date.past(),
    end: faker.date.recent(),
    quantity: 2,
    description:
      'ปวดหัว ตามัว ตัวร้อน ไม่ได้พักไม่ได้ผ่อน ผอมแห้งแรงน้อย กินไม่ได้ นอนไม่หลับ',
    certificate: 'มีใบรับรองแพทย์',
    _foreignKey: new ObjectID('601bb07bf4910f0f94c605b2')
  }
]

module.exports = json

const { validateCreateLibuser } = require('./validateCreateLibuser')
const { validateDeleteLibuser } = require('./validateDeleteLibuser')
const { validateGetLibuser } = require('./validateGetLibuser')
const { validateUpdateLibuser } = require('./validateUpdateLibuser')
const {validateCreateEducation} = require('./validateCreateEducation')
module.exports = {
  validateCreateLibuser,
  validateDeleteLibuser,
  validateGetLibuser,
  validateUpdateLibuser,
  validateCreateEducation
}

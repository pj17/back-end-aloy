const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const { roleAuthorization } = require('../controllers/auth')

const {
  getAllTrainings,
  getTrainings,
  createTraining,
  getTraining,
  updateTraining,
  deleteTraining,
  getUserTrainings
} = require('../controllers/training')

const {
  validateCreateTraining,
  validateGetTraining,
  validateUpdateTraining,
  validateDeleteTraining,
  validateGetUserTraining
} = require('../controllers/training/validators')

/*
 * Cities routes
 */

/*
 * Get all items route
 */
router.get('/all', getAllTrainings)
router.get(
  '/user/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateGetUserTraining,
  getUserTrainings
)

/*
 * Get items route
 */
router.get(
  '/',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  getTrainings
)

/*
 * Create new item route
 */
router.post(
  '/',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateCreateTraining,
  createTraining
)

/*
 * Get item route
 */
router.get(
  '/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateGetTraining,
  getTraining
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateUpdateTraining,
  updateTraining
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateDeleteTraining,
  deleteTraining
)

module.exports = router

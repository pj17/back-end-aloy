const uuid = require('uuid')
const Project = require('../../../models/project')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Creates a new item in database
 * @param {Object} req - request object
 */
const createItemInDb = ({
  name = '',
  description = ''
}) => {
  return new Promise((resolve, reject) => {
    const project = new Project({
      name,
      description,
      verification: uuid.v4()
    })
    project.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

      resolve(item)
    })
  })
}

module.exports = { createItemInDb }

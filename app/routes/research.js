const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const { roleAuthorization } = require('../controllers/auth')

const {
  getAllResearch,
  getResearch,
  getResearchs,
  createResearch,
  updateResearch,
  deleteResearch,
  getResearchUser
} = require('../controllers/research')

const {
  validateCreateResearch,
  validateGetResearch,
  validateUpdateResearch,
  validateDeleteResearch
} = require('../controllers/research/validators')

/*
 * Cities routes
 */

/*
 * Get all items route
 */
router.get('/all', getAllResearch)

/*
 * Get items route
 */
router.get(
  '/',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  getResearchs
)

/*
 * Create new item route
 */
router.post(
  '/',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateCreateResearch,
  createResearch
)

/*
 * Get item route
 */
router.get(
  '/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateGetResearch,
  getResearch
)

/*
 * Get item User route
 */
router.get(
  '/user/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateGetResearch,
  getResearchUser
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateUpdateResearch,
  updateResearch
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  validateDeleteResearch,
  deleteResearch
)

module.exports = router

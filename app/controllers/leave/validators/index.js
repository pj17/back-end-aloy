const { validateCreateLeave } = require('./validateCreateLeave')
const { validateDeleteLeave } = require('./validateDeleteLeave')
const { validateGetLeave } = require('./validateGetLeave')
const { validateUpdateLeave } = require('./validateUpdateLeave')

module.exports = {
  validateCreateLeave,
  validateDeleteLeave,
  validateGetLeave,
  validateUpdateLeave
}
